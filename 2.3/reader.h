/**
 * "reader.h" library
 *
 * @version 1.2
 * @author Ivan Udovin
 */

#ifndef __READER_H__

#define __READER_H__


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define __READER_VERSION 1


/**
 * Add library macro
 */

#define __READER_FAIL(x) { (void)fputs(x "\n", stderr); return __FALSE; }
#define __READER_FAIL_RANGE(x,l,r) { (void)fprintf(stderr, x "\n", l, r); return __FALSE; }

#define __BOOL int
#define __FALSE 0
#define __TRUE 1

#if (ULONG_MAX > 4294967295UL)
#define __LONG_64BIT
#endif


/**
 * Declaration
 */

/**
 * Reads one char from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_char(file, &c);
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_char(FILE *, /*@out@*/char *);

/**
 * Reads one long from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_long(file, &n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_long(FILE *, /*@out@*/long *);

/**
 * Reads one unsigned long from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_ulong(file, &n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_ulong(FILE *, /*@out@*/unsigned long *);

/**
 * Reads one double from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_double(file, &n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_double(FILE *, /*@out@*/double *);

/**
 * Reads one int from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_int(file, &n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_int(FILE *, /*@out@*/int *);

/**
 * Reads one unsigned int from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_uint(file, &n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_uint(FILE *, /*@out@*/unsigned int *);

/**
 * Reads one word from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_word(file, &str)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_word(FILE *, /*@out@*/char **);

/**
 * Reads one line from file
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fread_line(file, &str)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fread_line(FILE *, /*@out@*/char **);

/**
 * Reads one long from file and ensures that
 * the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fcread_long(file, &n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fcread_long(FILE *, /*@out@*/long *, long, long);

/**
 * Reads one unsigned long from file and ensures that
 * the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fcread_ulong(file, &n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fcread_ulong(FILE *, /*@out@*/unsigned long *, unsigned long, unsigned long);

/**
 * Reads one double from file and ensures that
 * the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fcread_double(file, &n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fcread_double(FILE *, /*@out@*/double *, double, double);

/**
 * Reads one int from file and ensures that
 * the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fcread_int(file, &n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fcread_int(FILE *, /*@out@*/int *, int, int);

/**
 * Reads one unsigned int from file and ensures that
 * the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: fcread_uint(file, &n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL fcread_uint(FILE *, /*@out@*/unsigned int *, unsigned int, unsigned int);

/**
 * Reads one char from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_char(&c);
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_char(/*@out@*/char *);

/**
 * Reads one long from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_long(&n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_long(/*@out@*/long *);

/**
 * Reads one unsigned long from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_ulong(&n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_ulong(/*@out@*/unsigned long *);

/**
 * Reads one double from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_double(&n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_double(/*@out@*/double *);

/**
 * Reads one int from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_int(&n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_int(/*@out@*/int *);

/**
 * Reads one unsigned int from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_uint(&n)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_uint(/*@out@*/unsigned int *);

/**
 * Reads one word from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_word(&str)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_word(/*@out@*/char **);

/**
 * Reads one line from standart input
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: read_line(&str)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL read_line(/*@out@*/char **);

/**
 * Reads one long from standart input and
 * ensures that the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: cread_long(&n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL cread_long(/*@out@*/long *, long, long);

/**
 * Reads one unsigned long from standart input and
 * ensures that the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: cread_ulong(&n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL cread_ulong(/*@out@*/unsigned long *, unsigned long, unsigned long);

/**
 * Reads one double from standart input and
 * ensures that the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: cread_double(&n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL cread_double(/*@out@*/double *, double, double);

/**
 * Reads one int from standart input and
 * ensures that the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: cread_int(&n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL cread_int(/*@out@*/int *, int, int);

/**
 * Reads one unsigned int from standart input and
 * ensures that the value is in range [lo, hi]
 * Returns status: __TRUE - valid, __FALSE - invalid
 * Example: cread_uint(&n, lo, hi)
 *
 * @return __BOOL
 */
/*@unused@*/__BOOL cread_uint(/*@out@*/unsigned int *, unsigned int, unsigned int);


/**
 * Implementation
 */

__BOOL fread_char(FILE *file, char *x)
{
	int c = 0;
	
	(*x) = (char)'\0';

	if (feof(file) != 0 || (c = fgetc(file)) == EOF)
		__READER_FAIL("Unexpected end of file");

	(*x) = (char)c;

	return __TRUE;
}

__BOOL fread_long(FILE *file, long *x)
{
	int len, i;
	__BOOL neg, zero;
	char *str = 0;
	long n;
	
	(*x) = (long)0;

	if (fread_word(file, &str) == 0)
		return __FALSE;
	
	if (str == 0)
		__READER_FAIL("Unknown error");

	if (*str != '+' && *str != '-' && (*str < '0' || *str > '9'))
	{
		free(str);
		__READER_FAIL("Expected integer");
	}

	len = 0, neg = __FALSE, zero = __TRUE;

	if (*str == '-')
		neg = __TRUE;
	else if (*str >= '0' && *str <= '9')
	{
		if (*str > '0')
			zero = __FALSE;
		len++;
	}

	for (i = 1; str[i] != '\0'; i++)
	{
		if (str[i] < '0' || str[i] > '9')
		{
			free(str);
			__READER_FAIL("Expected integer");
		}
		if (zero != 0)
		{
			len = 0;
			if (str[i] > '0')
				zero = __FALSE;
		}
		str[len++] = str[i];
	}

	if (zero != 0)
		neg = __FALSE;

	if (len == 0)
	{
		free(str);
		__READER_FAIL("Expected integer");
	}

#ifdef __LONG_64BIT
	if (len > 19)
#else
	if (len > 10)
#endif
	{
		free(str);
		__READER_FAIL_RANGE("Integer violates the range [%ld, %ld]", LONG_MIN, LONG_MAX);
	}

#ifdef __LONG_64BIT
	if (len == 19)
#else
	if (len == 10)
#endif
	{
#ifdef __LONG_64BIT
		if (neg == 0 && strcmp(str, "9223372036854775807") > 0)
#else
		if (neg == 0 && strcmp(str, "2147483647") > 0)
#endif
		{
			free(str);
			__READER_FAIL_RANGE("Integer violates the range [%ld, %ld]", LONG_MIN, LONG_MAX);
		}
#ifdef __LONG_64BIT
		if (neg != 0 && strcmp(str, "9223372036854775808") > 0)
#else
		if (neg != 0 && strcmp(str, "2147483648") > 0)
#endif
		{
			free(str);
			__READER_FAIL_RANGE("Integer violates the range [%ld, %ld]", LONG_MIN, LONG_MAX);
		}
	}

	for (i = 0, n = 0; i < len; i++)
		n = n * 10 + (neg != 0 ? -1 : 1) * (int)(str[i] - '0');

	free(str);

	(*x) = (long)n;

	return __TRUE;
}

__BOOL fread_ulong(FILE *file, unsigned long *x)
{
	int len, i;
	__BOOL neg, zero;
	char *str = 0;
	unsigned long n;
	
	(*x) = (unsigned long)0;

	if (fread_word(file, &str) == 0)
		return __FALSE;
	
	if (str == 0)
		__READER_FAIL("Unknown error");

	if (*str != '+' && *str != '-' && (*str < '0' || *str > '9'))
	{
		free(str);
		__READER_FAIL("Expected integer");
	}

	len = 0, neg = __FALSE, zero = __TRUE;

	if (*str == '-')
		neg = __TRUE;
	else if (*str >= '0' && *str <= '9')
	{
		if (*str > '0')
			zero = __FALSE;
		len++;
	}

	for (i = 1; str[i] != '\0'; i++)
	{
		if (str[i] < '0' || str[i] > '9')
		{
			free(str);
			__READER_FAIL("Expected integer");
		}
		if (zero != 0)
		{
			len = 0;
			if (str[i] > '0')
				zero = __FALSE;
		}
		str[len++] = str[i];
	}

	if (zero != 0)
		neg = __FALSE;

	if (len == 0)
	{
		free(str);
		__READER_FAIL("Expected integer");
	}

#ifdef __LONG_64BIT
	if (neg != 0 || len > 20)
#else
	if (neg != 0 || len > 10)
#endif
	{
		free(str);
		__READER_FAIL_RANGE("Integer violates the range [%d, %lu]", 0, ULONG_MAX);
	}

#ifdef __LONG_64BIT
	if (len == 20 && strcmp(str, "18446744073709551615") > 0)
#else
	if (len == 10 && strcmp(str, "4294967295") > 0)
#endif
	{
		free(str);
		__READER_FAIL_RANGE("Integer violates the range [%d, %lu]", 0, ULONG_MAX);
	}

	for (i = 0, n = 0; i < len; i++)
		n = n * 10 + (int)(str[i] - '0');

	free(str);

	(*x) = (unsigned long)n;

	return __TRUE;
}

__BOOL fread_double(FILE *file, double *x)
{
	int len, dlen, i;
	__BOOL neg, dot;
	char *str = 0;
	double n, d;
	
	(*x) = (double)0;
	
	if (fread_line(file, &str) == 0)
		return __FALSE;
	
	if (str == 0)
		__READER_FAIL("Unknown error");
	
	if (*str != '+' && *str != '-' && *str != '.' && (*str < '0' || *str > '9'))
	{
		free(str);
		__READER_FAIL("Expected decimal");
	}
	
	len = 0, dlen = 0, neg = __FALSE, dot = __FALSE;
	
	if (*str == '-')
		neg = __TRUE;
	else if (*str == '.')
		dot = __TRUE;
	else if (*str >= '0' && *str <= '9')
		len++;
	
	for (i = 1; str[i] != '\0'; i++)
	{
		if (str[i] == '.')
		{
			if (dot == __TRUE)
			{
				free(str);
				__READER_FAIL("Expected decimal");
			}
			dot = __TRUE;
		}
		else if (str[i] < '0' || str[i] > '9')
		{
			free(str);
			__READER_FAIL("Expected decimal");
		}
		else
		{
			str[len++] = str[i];
			if (dot == __TRUE)
				dlen++;
		}
	}
	
	if (len == 0)
	{
		free(str);
		__READER_FAIL("Expected decimal");
	}

	for (i = 0, n = 0; i < len - dlen; i++)
		n = n * 10 + (neg != 0 ? -1 : 1) * (int)(str[i] - '0');

	for (i = len - 1, d = 0; i >= len - dlen; i--)
		d = d / 10 + (neg != 0 ? -1 : 1) * (double)(str[i] - '0');
	
	free(str);
	
	(*x) = (double)(n + d / 10);
	
	return __TRUE;
}

__BOOL fread_int(FILE *file, int *x)
{
	long n = 0;
	
	(*x) = (int)0;

	if (fread_long(file, &n) == 0)
		return __FALSE;

	if (n < INT_MIN || n > INT_MAX)
		__READER_FAIL_RANGE("Integer violates the range [%d, %d]", INT_MIN, INT_MAX);

	(*x) = (int)n;

	return __TRUE;
}

__BOOL fread_uint(FILE *file, unsigned int *x)
{
	unsigned long n = 0;
	
	(*x) = (unsigned int)0;

	if (fread_ulong(file, &n) == 0)
		return __FALSE;

	if (n > UINT_MAX)
		__READER_FAIL_RANGE("Integer violates the range [%d, %u]", 0, UINT_MAX);

	(*x) = (unsigned int)n;

	return __TRUE;
}

__BOOL fread_word(FILE *file, char **str)
{
	int c, len, size;
	char *lstr;
	
	(*str) = (char *)malloc(1 * sizeof(char));
	
	if (*str != 0)
		strcpy(*str, "\0");

	while (feof(file) == 0 && (c = fgetc(file)) != EOF)
		if (c != (int)' ' && c != (int)'\n')
			break;

	if (feof(file) != 0 || c == EOF)
		__READER_FAIL("Unexpected end of file");

	len = 0, size = 1;
	
	if ((lstr = (char *)malloc(size * sizeof(char))) == 0)
		__READER_FAIL("Сould not allocate enough memory");

	lstr[len++] = (char)c;

	while (feof(file) == 0 && (c = fgetc(file)) != EOF)
	{
		if (c == (int)' ' || c == (int)'\n')
		{
			(void)ungetc(c, file);
			break;
		}
		if (len == size)
		{
			size = size > 0 ? size * 2 : 1;
			lstr = (char *)realloc(lstr, size * sizeof(char));
		}
		lstr[len++] = (char)c;
	}

	lstr = (char *)realloc(lstr, (len + 1) * sizeof(char));

	lstr[len] = '\0';

	if (len == 0)
	{
		free(lstr);
		__READER_FAIL_RANGE("Length of word violates the range [%d, %d]", 1, INT_MAX);
	}
	
	(*str) = (char *)realloc(*str, (len + 1) * sizeof(char));
	
	if (*str != 0)
		strcpy(*str, lstr);
	
	free(lstr);

	return __TRUE;
}

__BOOL fread_line(FILE *file, char **str)
{
	int c, len, size;
	char *lstr;

	(*str) = (char *)malloc(1 * sizeof(char));
	
	if (*str != 0)
		strcpy(*str, "\0");

	while (feof(file) == 0 && (c = fgetc(file)) != EOF)
		if (c != (int)'\n')
			break;

	if (feof(file) != 0 || c == EOF)
		__READER_FAIL("Unexpected end of file");

	len = 0, size = 1;
	
	if ((lstr = (char *)malloc(size * sizeof(char))) == 0)
		__READER_FAIL("Сould not allocate enough memory");

	lstr[len++] = (char)c;

	while (feof(file) == 0 && (c = fgetc(file)) != EOF)
	{
		if (c == (int)'\n')
			break;
		if (len == size)
		{
			size = size > 0 ? size * 2 : 1;
			lstr = (char *)realloc(lstr, size * sizeof(char));
		}
		lstr[len++] = (char)c;
	}

	lstr = (char *)realloc(lstr, (len + 1) * sizeof(char));

	lstr[len] = '\0';

	if (len == 0)
	{
		free(lstr);
		__READER_FAIL_RANGE("Length of line violates the range [%d, %d]", 1, INT_MAX);
	}
	
	(*str) = (char *)realloc(*str, (len + 1) * sizeof(char));
	
	if (*str != 0)
		strcpy(*str, lstr);
	
	free(lstr);

	return __TRUE;
}

__BOOL fcread_long(FILE *file, long *x, long lo, long hi)
{
	long n = 0;
	
	(*x) = (long)0;

	if (fread_long(file, &n) == 0)
		return __FALSE;

	if (n < lo || n > hi)
		__READER_FAIL_RANGE("Integer violates the range [%ld, %ld]", lo, hi);

	(*x) = (long)n;

	return __TRUE;
}

__BOOL fcread_ulong(FILE *file, unsigned long *x, unsigned long lo, unsigned long hi)
{
	unsigned long n = 0;
	
	(*x) = (unsigned long)0;

	if (fread_ulong(file, &n) == 0)
		return __FALSE;

	if (n < lo || n > hi)
		__READER_FAIL_RANGE("Integer violates the range [%lu, %lu]", lo, hi);

	(*x) = (unsigned long)n;

	return __TRUE;
}

__BOOL fcread_double(FILE *file, double *x, double lo, double hi)
{
	double n = 0;
	
	(*x) = (double)0;

	if (fread_double(file, &n) == 0)
		return __FALSE;

	if (n < lo || n > hi)
		__READER_FAIL_RANGE("Integer violates the range [%f, %f]", lo, hi);

	(*x) = (double)n;

	return __TRUE;
}

__BOOL fcread_int(FILE *file, int *x, int lo, int hi)
{
	int n = 0;
	
	(*x) = (int)0;

	if (fread_int(file, &n) == 0)
		return __FALSE;

	if (n < lo || n > hi)
		__READER_FAIL_RANGE("Integer violates the range [%d, %d]", lo, hi);

	(*x) = (int)n;

	return __TRUE;
}

__BOOL fcread_uint(FILE *file, unsigned int *x, unsigned int lo, unsigned int hi)
{
	unsigned int n = 0;
	
	(*x) = (unsigned int)0;

	if (fread_uint(file, &n) == 0)
		return __FALSE;

	if (n < lo || n > hi)
		__READER_FAIL_RANGE("Integer violates the range [%u, %u]", lo, hi);

	(*x) = (unsigned int)n;

	return __TRUE;
}

__BOOL read_char(char *x)
{
	return fread_char(stdin, x);
}

__BOOL read_long(long *x)
{
	return fread_long(stdin, x);
}

__BOOL read_ulogn(unsigned long *x)
{
	return fread_ulong(stdin, x);
}

__BOOL read_double(double *x)
{
	return fread_double(stdin, x);
}

__BOOL read_int(int *x)
{
	return fread_int(stdin, x);
}

__BOOL read_uint(unsigned int *x)
{
	return fread_uint(stdin, x);
}

__BOOL read_word(char **str)
{
	return fread_word(stdin, str);
}

__BOOL read_line(char **str)
{
	return fread_line(stdin, str);
}

__BOOL cread_long(long *x, long lo, long hi)
{
	return fcread_long(stdin, x, lo, hi);
}

__BOOL cread_ulong(unsigned long *x, unsigned long lo, unsigned long hi)
{
	return fcread_ulong(stdin, x, lo, hi);
}

__BOOL cread_double(double *x, double lo, double hi)
{
	return fcread_double(stdin, x, lo, hi);
}

__BOOL cread_int(int *x, int lo, int hi)
{
	return fcread_int(stdin, x, lo, hi);
}

__BOOL cread_uint(unsigned int *x, unsigned int lo, unsigned int hi)
{
	return fcread_uint(stdin, x, lo, hi);
}


/**
 * Remove library macro
 */

#undef __READER_FAIL
#undef __READER_FAIL_RANGE

#undef __BOOL
#undef __FALSE
#undef __TRUE

#undef __LONG_64BIT


#endif /* __READER_H__ */

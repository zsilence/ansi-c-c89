#include <stdio.h>
#include <math.h>
#include "reader.h"

static const double eps = 1e-7;
static double arr[12];
static double v[6];
static int input()
{
	int i;
	int arg_ind, segm_ind;
	for (i = 0; i < 12; i++) {
		segm_ind = i / 4 + 1;
		arg_ind = (i / 2) % 2 + 1;
		i % 2 == 0 ? printf("Enter coordinates of %d segment: x%d: ",
				    segm_ind,
				    arg_ind) :
		    printf("Enter coordinates of %d segment: y%d: ",
			   segm_ind, arg_ind);
		while(fread_double(stdin, &arr[i]) == 0);
	}
	return 1;
}

static int is_constructed()
{
	int i;
	for (i = 0; i < 6; i++) {
		v[i] = arr[2 * i + 2 - (i % 2)] - arr[2 * i - (i % 2)];
	}
	if (fabs(fabs(v[0] - v[2]) - fabs(v[4])) < eps
	    && fabs(fabs(v[1] - v[3]) - fabs(v[5])) < eps)
		return 1;
	else
		return 0;
}

static int triangle_type()
{

	double a, b, c, temp;
	a = sqrt(v[0] * v[0] + v[1] * v[1]);
	b = sqrt(v[2] * v[2] + v[3] * v[3]);
	c = sqrt(v[4] * v[4] + v[5] * v[5]);
	if (a > b) {
		temp = b;
		b = a;
		a = temp;
	}
	if (b > c) {
		temp = c;
		c = b;
		b = temp;
	}
	if (fabs(a - b) < eps && fabs(a - c) < eps)
		printf("Triangle is equilateral.\n\n");
	else {
		if (fabs(a - b) < eps || fabs(a - c) < eps || fabs(b - c) < eps)
			printf("Triangle is isosceles.\n\n");
		if (fabs((c * c) - (a * a + b * b)) < eps)
			printf("Triangle is right.\n\n");
	}
	return 0;
}

static double perimeter()
{
	double a, b, c;
	a = sqrt(v[0] * v[0] + v[1] * v[1]);
	b = sqrt(v[2] * v[2] + v[3] * v[3]);
	c = sqrt(v[4] * v[4] + v[5] * v[5]);
	return a + b + c;
}

static double area()
{
	return fabs(0.5 * (v[0] * v[5] - v[4] * v[1]));
}

int main()
{
	int i = 0;
	int input_check = 0;
	int constr_check = 0;
	printf("1. Enter segment's coordinates.\n"
	       "2. Check may triangle construct.\n"
	       "3. Determine triangle's type.\n"
	       "4. Calculate triangle's perimeter.\n"
	       "5. Calculate triangle's area.\n"
	       "6. Programm's information.\n" "7. Exit.\n");
	while (i != 7) {
		do {
			printf("Enter query number: ");
		} while ((read_int(&i) == 0) && (i > 0) && (i < 8));
		switch (i) {
		case 1:
			input_check = input();
			constr_check = 0;
			break;
		case 2:
			input_check == 1 ? constr_check =
			    is_constructed() : printf("Input coordinates\n\n");
			constr_check ==
			    1 ? printf("Triangle can be constructed.\n\n") :
			    printf("Triangle can not be constructed.\n\n");
			break;
		case 3:
			constr_check ==
			    1 ? triangle_type() :
			    printf("Check triangle can be constructed.\n\n");
			break;
		case 4:
			constr_check == 1 ? printf("Perimeter: %f\n\n",
						   perimeter()) :
			    printf("Check triangle can be constructed.\n\n");
			break;
		case 5:
			constr_check == 1 ? printf("Area: %f\n\n",
						   area()) :
			    printf("Check triangle can be constructed.\n\n");
			break;
		case 6:
			printf
			    ("Triangel v 1.0.\nDeveloped by Maksim Abramovich.\n\n");
			break;
		}
	}
	return 0;
}

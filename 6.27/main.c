#include <locale.h>
#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <string.h>

#define N 256
#define W 56

typedef struct node _node;

struct node {
	int cnt;
	wchar_t word[W];
	_node *left, *right;
};

static int cmp(wchar_t * a, wchar_t * b)
{
	int i = 0;
	for (; a[i] || b[i]; ++i)
		if (a[i] > b[i])
			return 1;
		else if (a[i] < b[i])
			return -1;
	return 0;
}

static int wpush(_node ** root, wchar_t * word)
{
	int i, j;
	if (!*root) {
		*root = calloc(1, sizeof(_node));
		if (!*root)
			return 1;
		for (i = 0; word[i]; i++)
			(*root)->word[i] = word[i];
		(*root)->cnt++;
		return 0;
	}
	j = cmp(word, (*root)->word);
	if (j == 1)
		wpush(&(*root)->right, word);
	else if (j == -1)
		wpush(&(*root)->left, word);
	else
		(*root)->cnt++;
	return 0;
}

static int npush(_node ** nroot, _node * node)
{
	int i;
	if (!*nroot) {
		*nroot = calloc(1, sizeof(_node));
		if (!*nroot)
			return 1;
		for (i = 0; node->word[i]; ++i)
			(*nroot)->word[i] = node->word[i];
		(*nroot)->cnt = node->cnt;
		return 0;
	}
	if (node->cnt > (*nroot)->cnt)
		npush(&(*nroot)->right, node);
	else
		npush(&(*nroot)->left, node);
	return 0;
}

static int record(_node * wtree, _node ** ntree)
{
	if (!wtree)
		return 0;
	if (record(wtree->left, ntree))
		return 1;
	if (record(wtree->right, ntree))
		return 1;
	if (npush(ntree, wtree))
		return 1;
	return 0;
}

static void traverse(_node * root)
{
	int i;
	if (!root)
		return;
	traverse(root->right);
	printf("%d ", root->cnt);
	for (i = 0; root->word[i]; ++i)
		printf("%lc", root->word[i]);
	printf("\n");
	traverse(root->left);
}

static void clean(_node * root)
{
	if (!root)
		return;
	clean(root->left);
	clean(root->right);
	free(root);
}

static int letter(wchar_t ch)
{
	return (ch >= L'А' && ch <= L'я') || ch == L'ё' || ch == L'Ё';
}

static int wread(wchar_t * s, _node ** wtree)
{
	int i, j = 0, word_ended;
	wchar_t *word = calloc(W, sizeof(wchar_t));
	if (!word)
		return 1;
	for (i = 0; s[i]; ++i) {
		if (letter(s[i]) && (j < W)) {
			word_ended = 0;
			word[j++] = s[i];
		} else
			word_ended = 1;
		if (word_ended && word[0]) {
			if (wpush(wtree, word))
				return 1;
			j = 0;
			free(word);
			word = calloc(W, sizeof(wchar_t));
			if (!word)
				return 1;
		}
	}
	free(word);
	return 0;
}

int main()
{
	wchar_t s[N];
	_node *wtree = NULL, *ntree = NULL;	
	FILE *f = fopen("text", "r");
    if(!f) 
    {
        printf("File not open.");
        return 0;
    }
	setlocale(LC_CTYPE, "");
	while (fgetws(s, N, f))
		if (wread(s, &wtree)) {
			printf("Memory can not be allocated.");
			return 0;
		}
	if (record(wtree, &ntree)) {
		printf("Memory can not be allocated.");
		return 0;
	}
	traverse(ntree);
	clean(ntree);
	clean(wtree);
	fclose(f);
	return 0;
}

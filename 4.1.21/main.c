#include <stdio.h>
#include "reader.h"

static int i = 0;
static int j = 0;

static void convert(int n, char s[], int b)
{
	if (n == 0)
		return;
	convert(n / b, s, b);
	s[i] = '0' + (char)(n % b);
	i++;
}

static void ItoB(int n, char s[], int b)
{
	if (n < 0) {
		n *= -1;
		s[i] = '-';
		i++;
	}
	convert(n, s, b);
	for (j = 0; j <= i; j++)
		if (s[j] > '9')
			s[j] += (char)7;
}

int main(void)
{
	int n;
	int b;
	char *s;
	s = calloc(32, sizeof(char));
	if (s == NULL) {
		printf("Memory can not be allocated.");
		return 0;
	}
	printf("Enter n: ");
	while (cread_int(&n, -2147483647, 2147483647) == 0) ;
	printf("Enter base b: ");
	while (cread_int(&b, 2, 36) == 0) ;
	ItoB(n, s, b);
	printf("Number %d in base %d: ", n, b);
	for (j = 0; j < i; j++)
		printf("%c", s[j]);
	free(s);
	return 0;
}

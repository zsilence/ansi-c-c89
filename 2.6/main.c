#include <stdio.h>
#include "reader.h"
#define PI 3.14159265358979323846264338327950288
#include <math.h>

static const double PI_2 = PI * 2;

static double _sin(double x, double eps)
{
	int i = 2;
	double result;
	double current;
	double sinc = sin(x);
    x= x - (double)((long)(x/PI_2) * PI_2);
	/*while ((x < -PI_2) || (x > PI_2)) {
		(x >= 0) ? (x -= PI_2) : (x += PI_2);
	}*/
	result = x;
	current = x;
	while (fabs(result - sinc) > eps) {
		current *= (-1.0) * x * x / (i * i + i);
		result += current;
		i += 2;
	}
	printf("Iterrations: %d\nResult: %f", i / 2, result);
	return result;
}

int main()
{
	double x;
	double eps;
	double sin_x;
	printf("Enter arg: ");
	while (fread_double(stdin, &x) == 0)
		printf("Enter arg: ");
	printf("Enter eps: ");
	while (fread_double(stdin, &eps) == 0)
		printf("Enter eps: ");
	sin_x = _sin(x, eps);
	eps = fabs(sin_x - sin(x));
	printf("\neps: %f", eps);
	return 0;
}

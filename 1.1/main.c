#include <stdio.h>
#include "reader.h"

static int is_valid(int n){
        return (n>99 && n<10000)? 1:0;
}


int main(){
    int n;
    printf("Enter three-digit or four-digit number: ");
    while ((read_int(&n) != 1) || (is_valid(n) != 1))
        printf("Try again: \n");
    
    if(n > 999)
    {
        int a = 0;
        while(n != 0)
        {
            a += n%10;
            n /= 10;
        }
        printf("sum of digits: %d", a);
    }
    else
    {
        int a = 1;
        while(n != 0)
        {
            a *= n%10;
            n /= 10;
        }
        printf("composition of digits: %d", a);
    }
	return 0;
}

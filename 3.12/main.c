#include <stdio.h>
#include "reader.h"

int main()
{
	int n = 1;
	int **arr = NULL;
	int i, j;
	printf("Enter n: ");
	while (cread_int(&n, 1, 28007) != 1)
		printf("Try again: ");
	arr = (int **)malloc(n * sizeof(int *));
	if (arr == NULL) {
	    printf("Memory can not be allocated.");
        return 0;
    }
	for (i = 0; i < n; i++) {
		arr[i] = (int *)malloc(n * sizeof(int));
		if (arr[i] == NULL) {
		    printf("Memory can not be allocated.");
            return 0;
        }
	}
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			arr[i][j] = (j + i) % n + 1;
		}
	}
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	for (i = 0; i < n; i++)
		free(arr[i]);
	free(arr);
	return 0;
}

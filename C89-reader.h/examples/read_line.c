#include <stdio.h>

// Include reader.h
#include "reader.h"

int main() {
	// Storage for string
	char *s;
	
	// Read string while not valid
	while (read_line(&s) == 0)
		free(s);
	
	// Print string
	puts(s);
	
	// Free storage for string
	free(s);
	
	return 0;
}

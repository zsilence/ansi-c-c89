#include <stdio.h>

// Include reader.h
#include "reader.h"

int main() {
	// Storage for number
	int n;
	
	// Read number while not valid
	while (read_int(&n) == 0);
	
	// Print number
	printf("%d", n);
	
	return 0;
}

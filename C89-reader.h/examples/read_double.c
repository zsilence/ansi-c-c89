#include <stdio.h>

// Include reader.h
#include "reader.h"

int main() {
	// Storage for number
	double n;

	// Read number while not valid
	while (fread_double(stdin, &n) == 0);

	// Print number
	printf("%f", n);

	return 0;
}

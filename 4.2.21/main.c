#include <stdio.h>
#include "reader.h"
#include <locale.h>
#include <wchar.h>


static int Letter(wchar_t ch)
{
	return (ch >= L'А' && ch <= L'я') || ch == L'ё' || ch == L'Ё';
}

static int Sign(wchar_t ch)
{				/*sign : { '!', '?', '.' , ',', ':', ';' } */
	return ch == L'!' || ch == L'?' || ch == L'.' || ch == L','
	    || ch == L';' || ch == L':';
}

static int SE(wchar_t ch)
{				/*statement end sign */
	return ch == L'.' || ch == L'!' || ch == L'?';
}

static wchar_t CL(wchar_t ch)
{				/*capital letter */
	if (ch >= L'а' && ch <= L'я')
		return ch - 0x20;
	if (ch >= L'А' && ch <= L'Я')
		return ch;
	if (ch == L'Ё')
		return ch;
	if (ch == L'ё')
		return L'Ё';
	return 0;
}

static wchar_t NL(wchar_t ch)
{				/*normal letter */
	if (ch >= L'а' && ch <= L'я')
		return ch;
	if (ch >= L'А' && ch <= L'Я')
		return ch + 0x20;
	if (ch == L'Ё')
		return L'ё';
	if (ch == L'ё')
		return ch;

	return 0;
}

static wchar_t *cs;
static void Correct(const wchar_t s[81])
{
	int i = 0;
	int j = 0;

	int word = 1, word_ended = 0;
	int c = 1;		/*capital */

	for (; s[i] != 0; i++)
		if (Letter(s[i])) {
			if (word_ended)
				cs[j++] = L' ';	/*set space here */
			if (c)
				cs[j++] = CL(s[i]);
			else
				cs[j++] = NL(s[i]);
			c = 0;
			word_ended = 0;
			word = 1;
		} else {
			if (word) {
				word = 0;
				word_ended = 1;
			}

			if (Sign(s[i])) {
				cs[j++] = s[i];
				if (SE(s[i]))
					c = 1;
			}
		}
	if (!c)
		cs[j++] = L'.';
	cs[j++] = L'\n';
	cs[j] = L'\0';
}

int main()
{
	wchar_t s[81];
	FILE *stream;
	FILE *stream2;
	int n = 81;

	setlocale(LC_CTYPE, "");

	stream = fopen("text.txt", "r");
	if (stream == NULL) {
		printf("Opening file error.\n");
		return 0;
	}

	stream2 = fopen("correct_text.txt", "w");
	if (stream2 == NULL) {
		printf("Opening file error.\n");
		return 0;
	}

	while (fgetws(s, n, stream)) {
		cs = malloc(sizeof(wchar_t) * 160);
		if (cs == NULL) {
			printf("Memory can not be allocated.\n");
			return 0;
		}

		Correct(s);

		if (fputws(cs, stream2) == EOF) {
			printf("Write line error.\n");
			return 0;
		}
		free(cs);
	}

	fclose(stream);
	fclose(stream2);

	return 0;
}

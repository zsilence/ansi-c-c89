#include <stdio.h>
#include "reader.h"
#include <time.h>

#define EQUAL 0
#define MORE 1
#define LESS 2

typedef struct item _item;
typedef struct lnumber _lnumber;

struct item {
	char digit;
	_item *prev;
	_item *next;
};

struct lnumber {
	int n;
	_item *head;
	_item *back;
};

static int push_back(_lnumber * a, char digit)
{
	_item *item = calloc(1, sizeof(_item));
	if (item == NULL) {
		printf("Memory can not be allocated.\n");
		return 1;
	}
	item->digit = digit;
	if (a->back)
		a->back->next = item;
	else
		a->head = item;
	item->prev = a->back;
	a->back = item;
	a->n++;
	return 0;
}

static void pop_head(_lnumber * a)
{
	_item *item = a->head;
	a->head = item->next;
	if (a->head)
		a->head->prev = NULL;
	else
		a->back = NULL;
	free(item);
	a->n--;
}

static void clean(_lnumber * a)
{
	while (a->back)
		pop_head(a);

}

static void write(_lnumber * a)
{
	_item *item = a->head;
	if (a->back)
		while (item != a->back->next) {
			printf("%c", item->digit);
			item = item->next;
		}
	printf("\n");
}

static void sub(_lnumber * a, _lnumber * b)
{
	char m = 0;
	_item *item_a = a->back;
	_item *item_b = b->back;
	while (item_b) {
		if (item_a->digit < (item_b->digit + m)) {
			item_a->digit += 10 - item_b->digit - m + '0';
			m = 1;
		} else {
			item_a->digit -= item_b->digit + m - '0';
			m = 0;
		}
		item_a = item_a->prev;
		item_b = item_b->prev;
	}
	while (m) {
		if (item_a->digit == '0')
			item_a->digit = '9';
		else {
			item_a->digit -= m;
			m = 0;
		}
		item_a = item_a->prev;
	}
	while (a->head && (a->head->digit == '0'))
		pop_head(a);

}

static void swap(_lnumber ** a, _lnumber ** b)
{
	_lnumber *c;
	c = *a;
	*a = *b;
	*b = c;
}

static int cmp(_lnumber * a, _lnumber * b)
{
	_item *itema;
	_item *itemb;
	if (a->n == b->n) {
		itema = a->head;
		itemb = b->head;
		while (itema != a->back->next) {
			if (itema->digit < itemb->digit)
				return LESS;
			else if (itema->digit > itemb->digit)
				return MORE;
			itema = itema->next;
			itemb = itemb->next;
		}
		return EQUAL;
	} else if (a->n < b->n)
		return LESS;
	return MORE;
}

static void mod(_lnumber * a, _lnumber * b, _lnumber * c)
{
	c->back = a->head;
	c->head = a->head;
	c->n = 1;
	do {
		while ((cmp(c, b) == LESS) && c->back->next) {
			c->back = c->back->next;
			c->n++;
		}
		while (cmp(c, b) != LESS)
			sub(c, b);
	} while (c->back && c->back->next);
	a->n = c->n;
	a->back = c->back;
	a->head = c->head;
}

static int gcd(_lnumber * a, _lnumber * b)
{
	_lnumber *c = calloc(1, sizeof(_lnumber));
	if (!c) {
		printf("Memory can not be allocated.");
		return 0;
	}
	do {
		mod(a, b, c);
		swap(&a, &b);
	}
	while (b->back);
	free(c);
	return 1;
}

static int is_valid(char *str)
{
	int i = -1;
	if (str[0] == '0')
		return 0;
	while (str[++i])
		if ((str[i] < '0') || (str[i] > '9'))
			return 0;
	return 1;
}

int main()
{
	int i = 0;
	char *stra, *strb;
	_lnumber *a = calloc(1, sizeof(_lnumber));
	_lnumber *b = calloc(1, sizeof(_lnumber));
	if (a == NULL) {
		printf("Memory can not be allocated.\n");
		return 0;
	}
	if (b == NULL) {
		printf("Memory can not be allocated.\n");
		return 0;
	}
	printf("Enter natural number a: ");
	while ((read_line(&stra) == 0) || (is_valid(stra) == 0)) {
		printf("Incorrect input. Try again: ");
		free(stra);
	}
	printf("Enter natural number b: ");
	while ((read_line(&strb) == 0) || (is_valid(strb) == 0)) {
		printf("Incorrect input. Try again: ");
		free(strb);
	}
	while (stra[i])
		if (push_back(a, stra[i++]))
			return 0;
	i = 0;
	while (strb[i])
		if (push_back(b, strb[i++]))
			return 0;
	if (!gcd(a, b))
		return 0;
	printf("GCD(a, b): ");
	if (cmp(a, b) == LESS)
		swap(&a, &b);
	write(a);
	clean(a);
	clean(b);
	free(a);
	free(b);
	free(stra);
	free(strb);
	return 0;
}
